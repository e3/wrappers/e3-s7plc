# Copyright (C) 2023 European Spallation Source ERIC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

USR_CPPFLAGS += -DUSE_TYPED_RSET

APP:=S7plcApp
APPSRC:=$(APP)/src

USR_INCLUDES += -I$(where_am_I)

SOURCES += $(APPSRC)/devS7plc.c
SOURCES += $(APPSRC)/drvS7plc.c

DBDS += $(APPSRC)/s7plcBase.dbd
DBDS += $(APPSRC)/s7plcCalcout.dbd
DBDS += $(APPSRC)/s7plcReg.dbd

SCRIPTS += ../iocsh/s7plc.iocsh

.PHONY: vlibs
vlibs:
